<?php
/**
 * Plugin Name: Calafou projects
 * Description: Add 'Project' as custom post type to wordpress admin UI
 * Version: 1.0
 * Author: Hackafou team
 * License: GPLv3
 */

/**
 *  Register 'project' custom post type
 */
function calafou_projects_custom_post_type()
{
   $labels = array(
       'name' => _x('Projects', 'post type general name'),
       'singular_name' => _x('Project', 'post type singular name'),
       'add_new' => _x('Add new', 'Project'),
       'add_new_item' => __('Add new project'),
       'edit_item' => __('Edit project'),
       'new_item' => __('New project'),
       'all_items' => __('All projects'),
       'view_item' => __('View project'),
       'search_items' => __('Search projects'),
       'not_found' => __('No projects found'),
       'not_found_in_trash' => __('No projects found in the Trash'),
       'parent_item_colon' => '',
       'menu_name' => 'Projects'
   );
   $args = array(
       'labels' => $labels,
       'description' => 'Displays projects',
       'public' => true,
       'menu_position' => 4,
       'supports' => array(
          'title',
          'author',
          'editor',
          'custom-fields',
          'thumbnail',
          'excerpt',
          'revisions',
          'comments'
       ),
       'rewrite'  => array('slug' => 'projects'),
       'has_archive' => true,
   );

   register_post_type('project', $args);
}
add_action('init', 'calafou_projects_custom_post_type' );
 
/**
 *  Register project taxonomy
 */
function calafou_projects_taxonomies()
{
    $labels = array(
        'name' => _x('Project categories', 'taxonomy general name'),
        'singular_name' => _x('Project category', 'taxonomy singular name'),
        'search_items' => __('Search Project categories'),
        'all_items' => __('All Project categories'),
        'parent_item' => __('Parent Project category'),
        'parent_item_colon' => __('Parent Project category:'),
        'edit_item' => __('Edit Project category'),
        'update_item' => __('Update Project category'),
        'add_new_item' => __('Add New Project category'),
        'new_item_name' => __('New Project category'),
        'menu_name' => __(' Project categories'),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
    );
    register_taxonomy('project_category', 'project', $args);
}
add_action('init', 'calafou_projects_taxonomies', 0 );

/**
 *  Register project taxonomy terms
 */
function calafou_projects_terms() {
    calafou_projects_taxonomies();
    $taxonomy = 'project_category';
    $terms = array (
        '0' => array (
            'name'          => __('Collective productive projects'),
            'slug'          => 'collective-productive-projects',
            'description'   => __('Lorem ipsum.'),
        ),
        '1' => array (
            'name'          => __('Autonomous productive projects'),
            'slug'          => 'autonomous-productive-projects',
            'description'   => __('Lorem ipsum.'),
        ),
        '2' => array (
            'name'          => __('Labs'),
            'slug'          => 'labs',
            'description'   => __('Lorem ipsum.'),
        ),
        '3' => array (
            'name'          => __('Collaborations'),
            'slug'          => 'collaborations',
            'description'   => __('Lorem ipsum.'),
        ),
    );
    foreach($terms as $term_key=>$term){
        if(!term_exists($term['name'], $taxonomy)){
            wp_insert_term(
                $term['name'],
                $taxonomy,
                array(
                    'description' => $term['description'],
                    'slug'        => $term['slug'],
                )
            );
            unset($term);
        }
    }
}

register_activation_hook( __FILE__, 'calafou_projects_terms' );
