<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package dazzling
 */

get_header(); ?>
	<div id="primary" class="content-area col-sm-12 col-md-12">
    <header class="entry-header page-header">
      <h1 class="entry-title"><?php wp_title(); ?></h1>
    </header><!-- .entry-header -->
		<main id="main" class="site-main row" role="main">
        <div class="col-md-4">
          <div class="projects-filter">
            <ul class="projects-filter__links sidebar-helper">
              <?php
              $terms = get_terms( array('taxonomy' => 'project_category', 'hide_empty' => false,));
              foreach($terms as $term_key=>$term):?>
              <li class="projects-filter__link" data-toggle-project-term="<?= $term->slug; ?>">
                <?= $term->name; ?>
              </li>
            <?php endforeach; ?>
            <li class="projects-filter__link" data-toggle-project-term="all">
              <?= __('Ver todos') ?>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-8">
			  <?php while ( have_posts() ) : the_post(); ?>
			    <?php get_template_part( 'content-without-title', 'page' ); ?>
    				<?php
      					if ( comments_open() || '0' != get_comments_number() ) :
      						comments_template();
      					endif;
      			?>
    		<?php endwhile; // end of the loop. ?>
        <?php
          $the_query = new WP_Query(array(
              'post_type'=> 'project',
              'order'    => 'ASC'
          ));
          if($the_query->have_posts()):?>
              <div class="projects-view">
                  <ul class="projects-view__items row masonry-grid">
                  <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
                      <?php $terms = wp_get_post_terms(get_the_ID(), 'project_category'); ?>
                      <a href="<?= the_permalink(); ?>">
                      <li class="projects-view__item project--short masonry-grid-item col-md-4 <?php foreach($terms as $term_key=>$term){ echo ($term->slug)." "; }; ?>">
                          <figure class="project__image--short">
                              <?= the_post_thumbnail() ?>
                          </figure>
                          <h3 class="project__title--short">
                                  <?= the_title() ?>
                          </h3>
                          <p class="project__excerpt--short">
                              <?= the_excerpt() ?>
                          </p>
                      </li>
                      </a>
                  <?php endwhile; // end of the loop. ?>
                  </ul>
              </div>
        <?php endif; // end of the loop. ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
