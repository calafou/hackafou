<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package dazzling
 */
?>
                </div><!-- close .row -->
            </div><!-- close .container -->
        </div><!-- close .site-content -->

	<div id="footer-area">
		<div class="container footer-inner">
			<?php get_sidebar( 'footer' ); ?>
		</div>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info container">
				<?php if( of_get_option('footer_social') ) dazzling_social_icons(); ?>
				<div class="col-md-3 address">
					   <div class="row">
                  <div class="col-md-6 icon">
                      <i class="fa fa-map-marker"></i>
                  </div>
                  <div class="col-md-6 text">
                      <p>Camí de Calafou, s/n</p>
                      <p>Vallbona d'Anoia, Barcelona</p>
                      <p>08785</p>
                  </div>
             </div>
				</div>
				<div class="col-md-3 contact">
          <div class="row">
               <div class="col-md-6 icon">
                  <i class="fa fa-envelope"></i>
               </div>
               <div class="col-md-6 text">
                 <a href="mailto:calafou@riseup.net">calafou@riseup.net</a>
               </div>
          </div>
				</div>
        <div class="col-md-3 links">
          <div class="row">
               <div class="col-md-6 icon">
                  <i class="fa fa-recycle"></i>
               </div>
               <div class="col-md-6 text">
                  Networks
               </div>
          </div>
				</div>
				<div class="col-md-3 licenses">
          <div class="row">
               <div class="col-md-6 icon">
                  <i class="fa fa-cc"></i>
               </div>
               <div class="col-md-6 text">
                    Licenses
               </div>
          </div>
				</div>
			</div><!-- .site-info -->
			<div class="scroll-to-top"><i class="fa fa-angle-up"></i></div><!-- .scroll-to-top -->
		</footer><!-- #colophon -->
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
