/**
 *   Hackafou Scripts
 */

!(function($){
$(document).ready(function(){

    /**
     *   Appends a navigation menu
     *   In a container with an attribute [data-navigation-id]
     *   That attribute must contain the html ID of the element
     *   you want to create a navigation for
     */
    var menu = $('[data-navigation-id]');
    if( menu.length > 0){
        var target_id = $('[data-navigation-id]').attr('data-navigation-id');
        var target    = $('#'+target_id);
        var links = document.createElement("ul");
        $('h5, h4, h3', target).each( function(){
            var link = document.createElement("li");
            $(link).text($(this).text());
            var y = $(this).offset().top;
            $(link).on('click', function(){
              $("html, body").animate({ scrollTop: y }, 600);
            })
            links.append(link);
        })
        menu.append(links)
    }

    /**
     *  Masonry grids
     */

    // init Masonry
    var $grid = $('.masonry-grid').masonry({
        itemSelector    : '.masonry-grid-item',
        columnWidth     : '.masonry-grid-item',
        percentPosition : true
    })

    // layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
        $grid.masonry('layout');
    });

    /**
     *   Project filters
     */
    var filters = $('[data-toggle-project-term]');
    if(filters.length > 0){
        filters.each(function(){
            $(this).click(function(){
                $('.projects-view__item').hide();
                var target = $(this).attr('data-toggle-project-term');
                $('[data-toggle-project-term]').removeClass('active');
                if(target != 'all'){
                  $('.'+target).show();
                  $('[data-toggle]').removeClass('active');
                  $(this).addClass('active');
                } else {
                  $('.projects-view__item').show();
                }
                $grid.masonry('layout');
            })
        })
    }

    /**
     *  Sticky sidebar helpers
     */
    var y_offset = 40; // offset from window top to helper
    var helper_top = $('.sidebar-helper').offset().top - y_offset;
    $(window).scroll(function(){
        var y = $(window).scrollTop();
        if(y >= helper_top){
            if(!$('.sidebar-helper').hasClass('fixed'))
                $('.sidebar-helper').addClass('fixed');
        } else if($('.sidebar-helper').hasClass('fixed')){
            $('.sidebar-helper').removeClass('fixed');
        }
    });
})
})(jQuery);
