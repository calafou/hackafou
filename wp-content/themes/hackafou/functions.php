<?php

// Add child theme styles
add_action( 'wp_enqueue_scripts', 'hackafou_enqueue_styles');
function hackafou_enqueue_styles(){

    $parent_style = 'dazzling-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style('hackafou-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    wp_enqueue_style('hackafou-custom-style',
        get_stylesheet_directory_uri() . '/style/css/style.css',
        array('hackafou-style'),
        wp_get_theme()->get('Version')
    );
}

// Add child theme scripts
add_action('wp_enqueue_scripts', 'hackafou_enqueue_scripts');
function hackafou_enqueue_scripts(){
    // Main JS file
    wp_register_script('hackafou', get_stylesheet_directory_uri().'/js/hackafou.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('hackafou');
    // Masonry file
    if(is_page('proyectos'))
    {
        wp_register_script('masonry', get_stylesheet_directory_uri().'/js/bower_components/masonry-layout/masonry.js', array(), '4.2.2', true);
        wp_enqueue_script('masonry');
        wp_register_script('imagesloaded', get_stylesheet_directory_uri().'/js/bower_components/imagesloaded/imagesloaded.pkgd.min.js', array('masonry'), '4.1.4', true);
        wp_enqueue_script('imagesloaded');
    }
}

// Add favicon to child theme
add_action('wp_head', 'calafou_favicon');
function calafou_favicon(){
    echo '<link href="'.get_stylesheet_directory_uri().'/favicon.ico" rel="shortcut icon">';
}

/**
 * Setup the WordPress core custom header feature.
 */
remove_action('after_setup_theme', 'dazzling_custom_header_setup');
function calafou_custom_header_setup(){
  	add_theme_support('custom-header', apply_filters( 'dazzling_custom_header_args', array(
  		'default-image'          => get_stylesheet_directory_uri().'/imgs/logo-calafou.svg',
  		'default-text-color'     => '000000',
  		'width'                  => 480,
  		'height'                 => 180,
  		'flex-height'            => true,
  		'wp-head-callback'       => 'dazzling_header_style',
  		'admin-head-callback'    => 'dazzling_admin_header_style',
  		'admin-preview-callback' => 'dazzling_admin_header_image',
  	)));
}
add_action('after_setup_theme', 'calafou_custom_header_setup');
